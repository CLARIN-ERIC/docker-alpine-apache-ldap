#!/bin/bash

#set -e

_APACHE_LOG_LEVEL="${APACHE_LOG_LEVEL:-info}"
_LDAP_NAME="${LDAP_NAME:-LDAP Authentication}"
_LDAP_URL="${LDAP_URL:-ldap://127.0.0.1/ou=people,dc=test}"
_LDAP_BIND_DN="${LDAP_BIND_DN:-cn=admin,ou=system}"
_LDAP_BIND_PASSWORD="${LDAP_BIND_PASSWORD:-verydifficultldappassword}"
_LDAP_REQUIRE="${LDAP_REQUIRE:-valid-user}"
_LDAP_VERIFY_SERVER_CERT=${LDAP_VERIFY_SERVER_CERT:-On}
_LDAP_TRUSTED_MODE=${LDAP_TRUSTED_MODE:-"SSL"}
_LDAP_TRUSTED_CERT_TYPE=${_LDAP_TRUSTED_CERT_TYPE:-CA_BASE64}
_LDAP_TRUSTED_CERT_FILE=${LDAP_TRUSTED_CERT_FILE:-/etc/apache2/vhosts/unity-ldap.pem}
_UNITY_HOST=${UNITY_HOST:-unity}
_UNITY_HTTPS_PORT=${UNITY_HTTPS_PORT:-2443}
_UNITY_LDAP_PORT=${UNITY_LDAP_PORT:-10000}

replaceVar() {
    replace "{{$1}}" "$2" "$3"
}

replace() {
    VAR=$(echo $1 | sed 's/\//\\\//g')
    VALUE=$(echo $2 | sed 's/\//\\\//g')
    FILE="$3"

    IS_REPLACE_NEEDED=$(grep -i "$VAR" "${FILE}" | wc -l)
    if [ "${IS_REPLACE_NEEDED}" != "0" ]; then
        IS_PASSWORD=$(echo "${VAR}" | grep -i "password" | wc -l)
        if [ "${IS_PASSWORD}" == "1" ]; then
            echo "  Replacing [$VAR] with [******]"
        else
            echo "  Replacing [$VAR] with [$VALUE]"
        fi

        sed -i "s/$VAR/$VALUE/g" "$FILE"
    fi
}

template_configuration() {
    echo "Updating apache configuration:"
    replaceVar "LDAP_NAME" "${_LDAP_NAME}" "/etc/apache2/vhosts/default.conf"
    replaceVar "LDAP_URL" "${_LDAP_URL}" "/etc/apache2/vhosts/default.conf"
    replaceVar "LDAP_BIND_DN" "${_LDAP_BIND_DN}" "/etc/apache2/vhosts/default.conf"
    replaceVar "LDAP_BIND_PASSWORD" "${_LDAP_BIND_PASSWORD}" "/etc/apache2/vhosts/default.conf"
    replaceVar "LDAP_REQUIRE" "${_LDAP_REQUIRE}" "/etc/apache2/vhosts/default.conf"
    replaceVar "LDAP_VERIFY_SERVER_CERT" ${_LDAP_VERIFY_SERVER_CERT} "/etc/apache2/vhosts/default.conf"
    replaceVar "LDAP_TRUSTED_MODE" ${_LDAP_TRUSTED_MODE} "/etc/apache2/vhosts/default.conf"
    if [ "${_LDAP_TRUSTED_MODE}" == "NONE" ]; then
      #Disable cert specific configuration
      sed -i "s/LDAPTrustedGlobalCert/#LDAPTrustedGlobalCert/g" "/etc/apache2/vhosts/default.conf"
      sed -i "s/LDAPVerifyServerCert/#LDAPVerifyServerCert/g" "/etc/apache2/vhosts/default.conf"
    else
      replaceVar "LDAP_TRUSTED_CERT_TYPE" ${_LDAP_TRUSTED_CERT_TYPE} "/etc/apache2/vhosts/default.conf"
      replaceVar "LDAP_TRUSTED_CERT_FILE" ${_LDAP_TRUSTED_CERT_FILE} "/etc/apache2/vhosts/default.conf"
    fi
    replace "LogLevel warn" "LogLevel ${_APACHE_LOG_LEVEL}" "/etc/apache2/httpd.conf"
    echo "Done"

    echo "Testing apache configuration:"
    httpd -t
    echo "Done"
}

start_apache() {
    echo "Starting apache:"
    httpd -DFOREGROUND
}

if [ "${_LDAP_TRUSTED_MODE}" != "NONE" ]; then
    echo 'Waiting for ldap directory to start'

    # shellcheck disable=SC2259
    while ! echo exit | curl --silent -v -k https://${_UNITY_HOST}:${_UNITY_HTTPS_PORT}/console </dev/null 2>/dev/null ; do
        echo 'Waiting for ldap directory to start'; sleep "1";
    done
    echo "Ldap directory started, continueing entrypoint script"

    echo "Downloading ldap directory certificate, mode=${_LDAP_TRUSTED_MODE}, host=${_UNITY_HOST}:${_UNITY_HTTPS_PORT} to /etc/apache2/vhosts/unity-ldap.pem"
    if [ "${_LDAP_TRUSTED_MODE}" == "SSL" ]; then
        echo "" | openssl s_client -connect ${_UNITY_HOST}:${_UNITY_HTTPS_PORT} -prexit 2>/dev/null | openssl x509 > /etc/apache2/vhosts/unity-ldap.pem
    elif [ "${_LDAP_TRUSTED_MODE}" == "STARTTLS" ]; then
        #echo "" | openssl s_client -connect ${_UNITY_HOST}:${_UNITY_HTTPS_PORT} -prexit 2>/dev/null | openssl x509 > /etc/apache2/vhosts/unity-ldap.pem
        echo "" | openssl s_client -connect ${_UNITY_HOST}:${_UNITY_LDAP_PORT} -starttls ldap -prexit 2>/dev/null | openssl x509 > /etc/apache2/vhosts/unity-ldap.pem
    fi
fi

template_configuration && start_apache